package edu.sjsu.android.project4WenboGuo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * A class for a SQLite database of locations.
 */
class LocationsDB extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "locationsDatabase";
    private static final String TABLE_NAME = "locations";
    private static final int VERSION = 1;
    // TODO: 4 Strings (protected static final) for 4 column names
    protected static final String COL_NAME_ID = "ID";
    protected static final String COL_NAME_LAT = "latitude";
    protected static final String COL_NAME_LON = "longitude";
    protected static final String COL_NAME_ZOOM = "zoomLevel";

    public LocationsDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String table = "Create Table " + TABLE_NAME + " ( " + COL_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME_LAT + " Double, " + COL_NAME_LON + " DOUBLE," + COL_NAME_ZOOM + " FLOAT)";

        try {
            sqLiteDatabase.execSQL(table);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * A method that inserts a new location to the table.
     *
     * @param contentValues location detail
     * @return the id
     */
    public long insert(ContentValues contentValues) {
        // TODO: insert the values to the table
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
    }

    /**
     * A method that deletes all locations from the table.
     *
     * @return number of locations deleted
     */
    public int deleteAll() {
         String res =  getReadableDatabase().compileStatement("Select count(*) from " + TABLE_NAME).simpleQueryForString();
         getWritableDatabase().execSQL("Delete from " + TABLE_NAME);
         return Integer.valueOf(res);
    }

    /**
     * A method that returns all the locations from the table.
     *
     * @return Cursor
     */
    public Cursor getAllLocations() {
        // TODO: query all data from the table
        return getWritableDatabase().query(TABLE_NAME, new String[]{COL_NAME_ID,COL_NAME_LAT,COL_NAME_LON,COL_NAME_ZOOM},null,null,null,null,null);
    }
}