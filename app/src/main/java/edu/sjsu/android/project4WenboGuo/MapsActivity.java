package edu.sjsu.android.project4WenboGuo;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import edu.sjsu.android.project4template.R;
import edu.sjsu.android.project4template.databinding.ActivityMapsBinding;
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {

    private GoogleMap mMap;
    private Uri uri = Uri.parse("content://edu.sjsu.android.project4WenboGuo.provider");
    private ActivityMapsBinding binding;
    private final LatLng LOCATION_CITY = new LatLng(37.3382,-121.8863);
    private final LatLng LOCATION_UNIV = new LatLng(37.335371, -121.881050);
    private final LatLng LOCATION_CS = new LatLng(37.333714, -121.881860);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        findViewById(R.id.uninstall).setOnClickListener(this::uninstall);
        findViewById(R.id.location).setOnClickListener(this::getLocation);
        findViewById(R.id.city).setOnClickListener(this::switchView);
        findViewById(R.id.university).setOnClickListener(this::switchView);
        findViewById(R.id.cs).setOnClickListener(this::switchView);

        LoaderManager.getInstance(this).restartLoader(0, null, this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(point -> {

            mMap.addMarker(new MarkerOptions().position(point).title("Heihei"));
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocationsDB.COL_NAME_LAT,point.latitude);
            contentValues.put(LocationsDB.COL_NAME_LON,point.longitude);
            contentValues.put(LocationsDB.COL_NAME_ZOOM,mMap.getCameraPosition().zoom);
            new MyTask().execute(contentValues);


        });

        mMap.setOnMapLongClickListener(point -> {
            // TODO: delete all locations from the database on long click

            mMap.clear();
            Toast.makeText(this, "All makers are removed",Toast.LENGTH_LONG).show();
            new MyTask().execute(new ContentValues());

        });
    }

    // Below is the class that extend AsyncTask, to insert/delete data in background
    // Note that AsyncTask is deprecated from API 30, but you can still use it.
    // You can use java.util.concurrent instead, if you are familiar with threads and concurrency.
    private class MyTask extends AsyncTask<ContentValues, Void, Void> {
        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            if(contentValues[0].get(LocationsDB.COL_NAME_LAT) == null){
                getContentResolver().delete(uri,null,null);
            }else{
                getContentResolver().insert(uri,contentValues[0]);
            }
            return null;
        }
    }
    // ----- End of AsyncTask classes -----


    // Below are for the CursorLoader, that is, the methods of
    // LoaderManager.LoaderCallbacks<Cursor> interface
    /**
     * Instantiate and return a new Loader for the database.
     *
     * @param id   the ID whose loader is to be created
     * @param args any arguments supplied by the caller
     * @return a new Loader instance that is ready to start loading
     */
    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this,this.uri,null,null,null,null);
    }

    /**
     * Draw the markers after data is loaded, that is, the loader returned
     * in the onCreateLoader has finished its load.
     *
     * @param loader the Loader that has finished
     * @param cursor a cursor to read the data generated by the Loader
     */
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

        if(cursor != null && cursor.moveToFirst()){
            while(cursor.moveToNext()){

                 drawMarker(cursor.getDouble(1),cursor.getDouble(2),cursor.getFloat(3),cursor.isLast());
            }
        }
    }
    private void drawMarker(double lan,double lon, float zoomLevel,boolean isLastLocation ){
        if(isLastLocation){
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lan,lon),zoomLevel));

        }

        this.mMap.addMarker(new MarkerOptions().position(new LatLng(lan,lon)));
    }
    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        // No need to implement
    }
    // ----- End of LoaderManager.LoaderCallbacks<Cursor> methods -----

    // Below are the methods respond to clicks on buttons
    // Remember to attach them to the buttons in onCreate
    // getLocation and switchView are the same as the ones in exercise 6.
    public void uninstall(View view) {
        startActivity(new Intent(Intent.ACTION_DELETE,Uri.parse("package:" + getPackageName())));
    }

    public void getLocation(View view) {
        GPSTracker tracker = new GPSTracker(this);
        tracker.getLocation();
    }

    public void switchView(View view) {
        CameraUpdateFactory cameraUpdateFactory;
        if(view.getId() == R.id.city){
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATION_CITY,10f));
        }else if(view.getId() == R.id.university){
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATION_UNIV,14f));
        }else{
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATION_CS,19f));
        }
    }
}